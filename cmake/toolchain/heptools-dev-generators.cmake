set(MCGENPATH  MCGenerators)

##LCG_external_package(lcgenv         1.3.2.1                              )
LCG_external_package(heputils       1.1.0          ${MCGENPATH}/heputils )

LCG_external_package(mcutils        1.2.1          ${MCGENPATH}/mcutils  heputils=1.1.0)
LCG_external_package(mcutils        1.3.0          ${MCGENPATH}/mcutils  heputils=1.1.0)

LCG_external_package(syscalc        1.1.4          ${MCGENPATH}/syscalc)
LCG_external_package(madgraph5amc       2.4.0          ${MCGENPATH}/madgraph5amc )
LCG_external_package(madgraph5amc       2.4.2          ${MCGENPATH}/madgraph5amc )

LCG_external_package(lhapdf            5.9.1          ${MCGENPATH}/lhapdf       )
LCG_external_package(lhapdf            6.1.5          ${MCGENPATH}/lhapdf       )
LCG_external_package(lhapdf            6.1.5.cxxstd   ${MCGENPATH}/lhapdf author=6.1.5 usecxxstd=1 )
LCG_external_package(lhapdf            6.1.6          ${MCGENPATH}/lhapdf       )
LCG_external_package(lhapdf            6.1.6.cxxstd   ${MCGENPATH}/lhapdf author=6.1.6 usecxxstd=1 )

LCG_external_package(lhapdfsets        5.9.1          lhapdfsets   )

LCG_external_package(powheg-box-v2         r3043.lhcb    ${MCGENPATH}/powheg-box-v2 author=r3043  )
LCG_external_package(powheg-box-v2     r3043.lhcb.rdynamic    ${MCGENPATH}/powheg-box-v2 author=r3043  )
LCG_external_package(powheg-box        r2092         ${MCGENPATH}/powheg-box       )

LCG_external_package(feynhiggs         2.8.6          ${MCGENPATH}/feynhiggs	)
LCG_external_package(feynhiggs         2.10.2         ${MCGENPATH}/feynhiggs	)
LCG_external_package(chaplin           1.2            ${MCGENPATH}/chaplin      )

LCG_external_package(pythia8           175            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           186            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           210            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           212            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           215            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           219            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           223            ${MCGENPATH}/pythia8 )

LCG_external_package(sacrifice         0.9.9          ${MCGENPATH}/sacrifice pythia8=186)

LCG_external_package(looptools         2.8            ${MCGENPATH}/looptools)

LCG_external_package(vbfnlo            3.0.0beta2     ${MCGENPATH}/vbfnlo feynhiggs=2.8.6)
LCG_external_package(FORM              4.1            ${MCGENPATH}/FORM)
#LCG_external_package(openloops         1.2.3          ${MCGENPATH}/openloops)
LCG_external_package(openloops         1.3.1          ${MCGENPATH}/openloops)
LCG_external_package(njet              2.0.0          ${MCGENPATH}/njet)
LCG_external_package(qgraf             3.1.4          ${MCGENPATH}/qgraf)
LCG_external_package(gosam_contrib     2.0            ${MCGENPATH}/gosam_contrib)
LCG_external_package(gosam             2.0.3          ${MCGENPATH}/gosam)

if (NOT (${LCG_TARGET} MATCHES "gcc62" OR ${LCG_OS} STREQUAL mac ) AND ${Python_native_version} VERSION_LESS 3)
LCG_external_package(thepeg            1.9.2p1        ${MCGENPATH}/thepeg author=1.9.2 rivet=2.4.2)
LCG_external_package(thepeg            2.0.2          ${MCGENPATH}/thepeg rivet=2.4.2)
endif()
LCG_external_package(thepeg            2.0.3          ${MCGENPATH}/thepeg rivet=2.5.2)

if (NOT (${LCG_TARGET} MATCHES "gcc62" OR ${LCG_OS} STREQUAL mac ) AND ${Python_native_version} VERSION_LESS 3)
LCG_external_package(herwig++          2.7.1          ${MCGENPATH}/herwig++  thepeg=1.9.2p1 )
LCG_external_package(herwig3           7.0.2          ${MCGENPATH}/herwig++  thepeg=2.0.2 lhapdf=6.1.6.cxxstd)
endif()
LCG_external_package(herwig3           7.0.3          ${MCGENPATH}/herwig++  thepeg=2.0.3 madgraph=2.4.2 openloops=1.3.1 lhapdf=6.1.6.cxxstd)
LCG_external_package(hjets             1.1-herwig-7.0.3  ${MCGENPATH}/hjets herwig=7.0.3 author=1.1)

LCG_external_package(tauola++          1.1.1a         ${MCGENPATH}/tauola++     )
LCG_external_package(tauola++          1.1.4          ${MCGENPATH}/tauola++     )
LCG_external_package(tauola++          1.1.5          ${MCGENPATH}/tauola++     )
LCG_external_package(tauola++          1.1.6          ${MCGENPATH}/tauola++     )

LCG_external_package(pythia6           427.2          ${MCGENPATH}/pythia6    author=6.4.27 hepevt=10000  )
LCG_external_package(pythia6           428.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=10000  )
LCG_external_package(pythia6           429.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=10000  )

LCG_external_package(agile             1.4.1          ${MCGENPATH}/agile        )

LCG_external_package(photos++          3.56           ${MCGENPATH}/photos++     )
LCG_external_package(photos++          3.61           ${MCGENPATH}/photos++     )

LCG_external_package(photos            215.4          ${MCGENPATH}/photos       )

if (NOT (${LCG_OS} STREQUAL mac))
  LCG_external_package(evtgen            1.2.0          ${MCGENPATH}/evtgen       tag=R01-02-00 pythia8=175 tauola++=1.1.1a)
endif()
LCG_external_package(evtgen            1.3.0          ${MCGENPATH}/evtgen       tag=R01-03-00 pythia8=186 tauola++=1.1.4)
LCG_external_package(evtgen            1.4.0          ${MCGENPATH}/evtgen       tag=R01-04-00 pythia8=186 tauola++=1.1.4)
LCG_external_package(evtgen            1.5.0          ${MCGENPATH}/evtgen       tag=R01-05-00 pythia8=212 tauola++=1.1.5)

if (NOT (${LCG_TARGET} MATCHES "gcc62"))
  LCG_external_package(rivet             2.4.3          ${MCGENPATH}/rivet        yoda=1.6.1      )
  LCG_external_package(rivet             2.5.1          ${MCGENPATH}/rivet        yoda=1.6.3      )
  if(${Python_native_version} VERSION_LESS 3)
    LCG_external_package(rivet             2.4.2          ${MCGENPATH}/rivet        yoda=1.5.9      )
  endif()
endif()
LCG_external_package(rivet             2.5.3          ${MCGENPATH}/rivet        yoda=1.6.6      )
LCG_external_package(rivet             2.5.2          ${MCGENPATH}/rivet        yoda=1.6.5      )

LCG_external_package(sherpa            2.1.1          ${MCGENPATH}/sherpa       author=2.1.1 hepevt=10000)
LCG_external_package(sherpa            2.2.1          ${MCGENPATH}/sherpa       author=2.2.1 hepevt=10000)
LCG_external_package(sherpa            2.2.2          ${MCGENPATH}/sherpa       author=2.2.2 hepevt=10000)
LCG_external_package(sherpa            2.2.0          ${MCGENPATH}/sherpa       author=2.2.0 hepevt=10000)

LCG_external_package(qd                2.3.13         ${MCGENPATH}/qd          )

if (NOT (${LCG_HOST_ARCH} STREQUAL i686 OR ${LCG_OS} STREQUAL mac OR ${LCG_TARGET} MATCHES "clang" OR ${LCG_TARGET} MATCHES "gcc62"))
    LCG_external_package(blackhat           0.9.9         ${MCGENPATH}/blackhat    )
    LCG_external_package(sherpa-mpich2     2.1.1          ${MCGENPATH}/sherpa-mpich2  author=2.1.1 hepevt=10000)
    LCG_external_package(sherpa            2.2.0.blackhat ${MCGENPATH}/sherpa         author=2.2.0 hepevt=10000 lhapdf=6.1.6)
endif()

LCG_external_package(hepmcanalysis     3.4.14         ${MCGENPATH}/hepmcanalysis  author=00-03-04-14 )
LCG_external_package(mctester          1.25.0         ${MCGENPATH}/mctester     )
LCG_external_package(hijing            1.383bs.2      ${MCGENPATH}/hijing       )
LCG_external_package(starlight         r43            ${MCGENPATH}/starlight    )
LCG_external_package(starlight         r193            ${MCGENPATH}/starlight   )

LCG_external_package(herwig            6.520.2        ${MCGENPATH}/herwig       )
LCG_external_package(herwig            6.521.2        ${MCGENPATH}/herwig       )

LCG_external_package(crmc              1.5.4          ${MCGENPATH}/crmc         )
LCG_external_package(crmc              1.5.6          ${MCGENPATH}/crmc         )

LCG_external_package(yoda              1.6.1          ${MCGENPATH}/yoda  cython=0.23.4     )
LCG_external_package(yoda              1.6.3          ${MCGENPATH}/yoda  cython=0.23.4     )
LCG_external_package(yoda              1.6.4          ${MCGENPATH}/yoda  cython=0.23.4     )
LCG_external_package(yoda              1.6.6          ${MCGENPATH}/yoda  cython=0.23.4     )
LCG_external_package(yoda              1.6.5          ${MCGENPATH}/yoda  cython=0.23.4     )
if(${Python_native_version} VERSION_LESS 3)
    LCG_external_package(yoda              1.5.5          ${MCGENPATH}/yoda  cython=0.23.4     )
    LCG_external_package(yoda              1.5.9          ${MCGENPATH}/yoda  cython=0.23.4     )
endif()

LCG_external_package(hydjet            1.6            ${MCGENPATH}/hydjet author=1_6 )
LCG_external_package(hydjet            1.8            ${MCGENPATH}/hydjet author=1_8 )
LCG_external_package(tauola            28.121.2       ${MCGENPATH}/tauola       )

LCG_external_package(jimmy             4.31.3         ${MCGENPATH}/jimmy        )
LCG_external_package(hydjet++          2.1            ${MCGENPATH}/hydjet++ author=2_1)
LCG_external_package(alpgen            2.1.4          ${MCGENPATH}/alpgen author=214 )
LCG_external_package(pyquen            1.5.1          ${MCGENPATH}/pyquen author=1_5)
LCG_external_package(baurmc            1.0            ${MCGENPATH}/baurmc       )
LCG_external_package(professor         1.4.0          ${MCGENPATH}/professor    )
LCG_external_package(professor         2.1.3          ${MCGENPATH}/professor    )

LCG_external_package(jhu               5.6.3          ${MCGENPATH}/jhu          )

LCG_external_package(vincia            1.2.01          ${MCGENPATH}/vincia pythia8=215)

#LCG_external_package(fastnlo_toolkit   2.3.1pre-1871  ${MCGENPATH}/fastnlo_toolkit )
